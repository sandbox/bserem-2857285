INTRODUCTION:
This module acts as a helper to `mass_contact` module. It automates the creation
of mass contact categories, based on taxonomy terms, when a new term is being
added to the site.

The module adds categories automatically for terms that get added to a
vocabulary that is being referenced from the user entity.
The user adding the term has an option to select whether to create a category or not.

For more information on taxonomy based categories see the `mass_contact` module
documendation.

INSTALLATION:
Install as any other contrib module.

CONFIGURATION:
This module has no UI nor configuration at the moment. It does a fairly simple
job and nothing else.

CREDITS:
Developed by Bill Seremetis (bill@seremetis.net) for zehnplus.ch
Special thanks to the developers of the `mass_contact` module.
